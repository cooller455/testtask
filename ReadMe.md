#Настройки
в файле resources/application.properties
- можно указать доступы к базе - по-умолчанию:
   - **spring.datasource.url** база: testTask
   - **spring.datasource.username** логин: postgres 
   - **spring.datasource.password** пароль: 
- можно указать частоту обновления данных 
    - **test.task.fixed.delay.seconds** - 360 по-умолчанию
- можно указать порт на котором будт работать приложение
    - **server.port** - 8080 по-умолчанию
# Сборка и запуск
- если не установлен - установить сборщик Maven
- выполнить mvn install
- для запуска выполнить java -jar FILE.jar, где FILE.jar файл полученный в результате выполнения предыдущего пункта

# API
Вместо TEXT подставить нужные символы
- http://localhost:8080/rest/code?country=TEXT - вернет страны начинающиеся с TEXT
- http://localhost:8080/rest/contain?country=TEXT  - вернет страны содрежащие с TEXT