package com.magnitsky.test.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(uniqueConstraints = { @UniqueConstraint( columnNames = { "name"} ) } )
public class CountryCode {
    @Id
    String name;
    String country;
    String code;

}
