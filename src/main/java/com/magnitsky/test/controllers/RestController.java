package com.magnitsky.test.controllers;

import com.magnitsky.test.models.CountryCode;
import com.magnitsky.test.services.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.sql.SQLException;
import java.util.List;


@Controller
public class RestController {

    @Autowired
    private CacheService cacheService;

    @GetMapping(path = "/rest/code")
    public @ResponseBody
    List<CountryCode> findByName(@RequestParam String country){
        return cacheService.findByName(country);
    }

    @GetMapping(path = "/rest/contain")
    public @ResponseBody
    List<CountryCode> findByString(@RequestParam String country){
        return cacheService.findByString(country);
    }



}
