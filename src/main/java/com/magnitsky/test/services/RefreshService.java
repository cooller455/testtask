package com.magnitsky.test.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magnitsky.test.models.CountryCode;
import com.magnitsky.test.repository.CountryCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

/*
* Сервис для обновления данных
* */
@EnableScheduling
@Service
public class RefreshService {
private static long counter = 0;
    @Autowired
    private CacheService cacheService;

    private boolean needBaseRefresh = false;

    @Autowired
    private CountryCodeRepository countryCodeRepository;

    @PostConstruct
    public void initialize() {
        refresh();
    }
/*
* Обновляет данные каждые test.task.fixed.delay.seconds секунд(360 - по-умолчанию)
* */
    @Scheduled(fixedRateString = "${test.task.fixed.delay.seconds:360}000")
    public void refresh(){
        //Обновляем имена
        final String names = getInfo("http://country.io/names.json");
        refreshNames(names);

        //Обновляем коды
        final String codes = getInfo("http://country.io/phone.json");
        refreshCodes(codes);

        //Если значения изменились - обновляем значения в БД
        if(needBaseRefresh){
            countryCodeRepository.saveAll(cacheService.getCache());
            needBaseRefresh = false;
        }
    }

    private void refreshCodes(String codes) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final Map<String, String> map = mapper.readValue(codes, new TypeReference<Map<String,String>>(){});
            //Проверяем наличие страны в кэше
            map.forEach((k,v)->{
                final Optional<CountryCode> cntry = cacheService.getCache().stream().filter(c -> c.getName().equals(k)).findFirst();
                if(cntry.isPresent()){
                    if(cntry.get().getName().equals(k) && !v.equals(cntry.get().getCode())){
                        cntry.get().setCode(v);
                        needBaseRefresh = true;
                    }
                }else{
                    CountryCode cp = CountryCode.builder().name(k).code(v).build();
                    cacheService.getCache().add(cp);
                    needBaseRefresh = true;
                }
            });
        }catch (IOException e){
            System.err.println(e);
        }
    }

    private void refreshNames(String names) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final Map<String, String> map = mapper.readValue(names, new TypeReference<Map<String,String>>(){});
            //Проверяем наличие страны в кэше
            map.forEach((k,v)->{
                final Optional<CountryCode> cntry = cacheService.getCache().stream().filter(c -> c.getName().equals(k)).findFirst();
                //Если есть
                if(cntry.isPresent()){
                    if(cntry.get().getName().equals(k) && !v.equals(cntry.get().getCode())){
                        cntry.get().setCountry(v);
                        needBaseRefresh = true;
                    }
                }else{
                    CountryCode cp = CountryCode.builder().name(k).country(v).build();
                    cacheService.getCache().add(cp);
                    needBaseRefresh = true;
                }
            });
        }catch (IOException e){
            System.err.println(e);
        }
    }


    private String getInfo(String url) {
        final RestTemplate restTemplate = new RestTemplate();

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        final HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        try{
            final ResponseEntity<String> s = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

            if(s.getStatusCode().is2xxSuccessful()){
                return s.getBody();
            }
        }catch (HttpClientErrorException e){
            System.out.println(e);
        }

        return "";
    }

}
