package com.magnitsky.test.services;

import com.magnitsky.test.models.CountryCode;
import com.magnitsky.test.repository.CountryCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
* Сервис отвечает за работу с кэшем
* */
@Service
public class CacheService {
    @Autowired
    private CountryCodeRepository countryCodeRepository;

    private final List<CountryCode> cache = new ArrayList<>();

    public List<CountryCode> getCache(){
        return cache;
    }

    public List<CountryCode> findByString(String s){
        return this.getCache().stream().filter(c->c.getCountry().toLowerCase().contains(s.toLowerCase())).collect(Collectors.toList());
    }

    public List<CountryCode> findByName(String s){
        return this.getCache().stream().filter(c->c.getCountry().toLowerCase().startsWith(s.toLowerCase())).collect(Collectors.toList());
    }

    @PostConstruct
    public void initialize() {
        this.getCache().addAll(countryCodeRepository.findAll());
    }


}
