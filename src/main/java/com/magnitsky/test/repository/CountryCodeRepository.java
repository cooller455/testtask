package com.magnitsky.test.repository;

import com.magnitsky.test.models.CountryCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryCodeRepository extends JpaRepository<CountryCode, String> {
}
